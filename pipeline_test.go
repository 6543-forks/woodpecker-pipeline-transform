// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package transform_test

import (
	"testing"

	transform "codeberg.org/lafriks/woodpecker-pipeline-transform"

	"github.com/goccy/go-yaml"
	"github.com/stretchr/testify/assert"
)

func TestPipelineUnmarshal(t *testing.T) {
	p := transform.Pipeline{
		Steps: transform.Steps{
			&transform.Step{
				Name:     "step1",
				Image:    "alpine:latest",
				Commands: []string{"echo Step 1", "echo $${CI_JOB_ID}"},
				When: &transform.When{
					Event: []string{"push", "pull_request"},
					Branch: &transform.Conditions{
						Conditions: []string{"main"},
					},
				},
			},
			&transform.Step{
				Name:  "step2",
				Image: "alpine:latest",
			},
		},
	}

	buf, err := yaml.Marshal(&p)
	assert.NoError(t, err)
	assert.Equal(t, `pipeline:
  step1:
    image: alpine:latest
    commands:
    - echo Step 1
    - echo $${CI_JOB_ID}
    when:
      branch: main
      event:
      - push
      - pull_request
  step2:
    image: alpine:latest
`, string(buf))
}
