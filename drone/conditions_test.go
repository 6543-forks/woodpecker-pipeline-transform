// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package drone

import (
	"testing"

	"codeberg.org/lafriks/woodpecker-pipeline-transform/core"
	"github.com/goccy/go-yaml"
	"github.com/stretchr/testify/assert"
)

func TestConditionUnmarshal(t *testing.T) {
	v := &struct {
		Refs *Conditions `yaml:"refs"`
	}{}
	err := yaml.Unmarshal([]byte("refs:\n- test1\n- test2\n"), v)
	assert.NoError(t, err)
	assert.ElementsMatch(t, core.Strings{"test1", "test2"}, v.Refs.Conditions)
}
