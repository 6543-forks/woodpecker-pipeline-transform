// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package transform

import (
	"codeberg.org/lafriks/woodpecker-pipeline-transform/core"
)

type Conditions struct {
	Conditions []string `yaml:"-"`
	Include    []string `yaml:"include,omitempty"`
	Exclude    []string `yaml:"exclude,omitempty"`
}

func (c *Conditions) IsEmpty() bool {
	if c == nil {
		return true
	}
	return len(c.Conditions) == 0 && len(c.Include) == 0 && len(c.Exclude) == 0
}

func (c Conditions) MarshalYAML() (interface{}, error) {
	if len(c.Conditions) > 0 {
		if len(c.Conditions) == 1 {
			return c.Conditions[0], nil
		}
		return c.Conditions, nil
	}
	return &struct {
		Include []string `yaml:"include,omitempty"`
		Exclude []string `yaml:"exclude,omitempty"`
	}{
		Include: c.Include,
		Exclude: c.Exclude,
	}, nil
}

type PathConditions struct {
	Conditions    []string `yaml:"-"`
	Include       []string `yaml:"include,omitempty"`
	Exclude       []string `yaml:"exclude,omitempty"`
	IgnoreMessage string   `yaml:"ignore_message,omitempty"`
}

func (c *PathConditions) IsEmpty() bool {
	if c == nil {
		return true
	}
	return len(c.Conditions) == 0 && len(c.Include) == 0 && len(c.Exclude) == 0
}

func (c PathConditions) MarshalYAML() (interface{}, error) {
	if len(c.Conditions) > 0 {
		if len(c.Conditions) == 1 {
			return c.Conditions[0], nil
		}
		return c.Conditions, nil
	}
	return &struct {
		Include       []string `yaml:"include,omitempty"`
		Exclude       []string `yaml:"exclude,omitempty"`
		IgnoreMessage string   `yaml:"ignore_message,omitempty"`
	}{
		Include:       c.Include,
		Exclude:       c.Exclude,
		IgnoreMessage: c.IgnoreMessage,
	}, nil
}

type When struct {
	Repo        core.Strings      `yaml:"repo,omitempty"`
	Branch      *Conditions       `yaml:"branch,omitempty"`
	Event       core.Strings      `yaml:"event,omitempty"`
	Tag         string            `yaml:"tag,omitempty"`
	Status      []string          `yaml:"status,omitempty"`
	Platform    core.Strings      `yaml:"platform,omitempty"`
	Environment string            `yaml:"environment,omitempty"`
	Matrix      map[string]string `yaml:"matrix,omitempty"`
	Instance    string            `yaml:"instance,omitempty"`
	Path        *PathConditions   `yaml:"path,omitempty"`
}

func (s When) IsEmpty() bool {
	return len(s.Repo) == 0 &&
		s.Branch.IsEmpty() &&
		len(s.Event) == 0 &&
		s.Tag == "" &&
		len(s.Status) == 0 &&
		len(s.Platform) == 0 &&
		s.Environment == "" &&
		len(s.Matrix) == 0 &&
		s.Instance == "" &&
		s.Path.IsEmpty()
}
