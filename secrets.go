// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package transform

import (
	"strings"
)

type Secret struct {
	Source string `yaml:"source"`
	Target string `yaml:"target"`
}

type Secrets []Secret

func (s Secrets) MarshalYAML() (interface{}, error) {
	arr := make([]interface{}, 0, len(s))
	for _, secret := range s {
		if secret.Target != "" && !strings.EqualFold(secret.Source, secret.Target) {
			arr = append(arr, Secret{
				Source: secret.Source,
				Target: strings.ToLower(secret.Target),
			})
		} else {
			arr = append(arr, secret.Source)
		}
	}
	return arr, nil
}
