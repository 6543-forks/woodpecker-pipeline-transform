// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package transform

import (
	"github.com/goccy/go-yaml"
)

type Setting struct {
	Name  string
	Value interface{}
}

type Settings []Setting

func (s Settings) MarshalYAML() (interface{}, error) {
	v := make(yaml.MapSlice, len(s))
	for i, setting := range s {
		v[i] = yaml.MapItem{
			Key:   setting.Name,
			Value: setting.Value,
		}
	}
	return v, nil
}

func (s *Settings) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var v yaml.MapSlice
	if err := unmarshal(&v); err != nil {
		return err
	}
	val := make(Settings, 0, len(v))
	for _, item := range v {
		val = append(val, Setting{
			Name:  item.Key.(string),
			Value: item.Value,
		})
	}
	*s = val
	return nil
}
